from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from Account.urls import router as auth_router
from Chat.urls import router as chat_router
from Event.urls import router as event_router
from Host.urls import router as host_router
from Notification.urls import router as notification_router
from Request.urls import router as request_router
from Travel.urls import router as travel_router

router = DefaultRouter()

router.registry.extend(auth_router.registry)
router.registry.extend(chat_router.registry)
router.registry.extend(event_router.registry)
router.registry.extend(host_router.registry)
router.registry.extend(notification_router.registry)
router.registry.extend(request_router.registry)
router.registry.extend(travel_router.registry)

urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('debug/', include(debug_toolbar.urls)),
    ] + urlpatterns