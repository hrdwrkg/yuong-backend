from Bot.views import Bot


@Bot.listener.on("Bot.Reply")
def message_(msg):
    user_id = msg.get('from').get('id')
    message = "Message"
    Bot.sends(user_id, 'message', '{}'.format(message))


@Bot.listener.on("Bot.Command")
def command_(msg):
    command = msg.get('text')
    user_id = msg.get('from').get('id')
    if command == '/start':
        message = 'Welcome'
        Bot.sends(user_id, 'message', message)
    return

