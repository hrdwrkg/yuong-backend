import telepot
from django.apps import AppConfig


class BotConfig(AppConfig):
    name = 'Bot'
    token = ''
    bot = telepot.Bot(token)
    host = 'http://localhost:8000'
    url = 'https://telegram.me/yuong_bot'

    text = {}

