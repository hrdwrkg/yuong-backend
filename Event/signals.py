from django.db.models.signals import post_save
from django.dispatch import receiver

from Event.models import Event


@receiver(post_save, sender=Event)
def add_owner(instance, created, **kwargs):
    if created:
        instance.members.add(instance.owner)
        instance.save()