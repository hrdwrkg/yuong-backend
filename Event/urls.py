from rest_framework.routers import DefaultRouter

from Event.views import EventViewSet

router = DefaultRouter()

router.register(r'event', EventViewSet, base_name='event')