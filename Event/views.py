from rest_framework import viewsets, mixins
from rest_framework.decorators import detail_route
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK

from Event.models import Event
from Event.serializers import EventSerializer, EventJoinSerializer, EventUpdateSerializer


class EventViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.UpdateModelMixin,
                  mixins.RetrieveModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def get_permissions(self):
        if self.action in ['create', 'update', 'partial_update', 'destroy']:
            self.permission_classes = [IsAuthenticated]
        elif self.action in ['list', 'retrieve']:
            self.permission_classes = [AllowAny]
            
        return super(EventViewSet, self).get_permissions()

    def create(self, request, *args, **kwargs):
        serializer = EventSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        serializer.save(owner=request.user)

        return Response(serializer.data, status=HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = EventUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        event = Event.objects.get(id=kwargs['pk'])

        if event.owner != request.user:
            return Response({'status': 400, 'text': 'you don\'t have permission'},
                            status=HTTP_400_BAD_REQUEST)

        serializer.update(event, serializer.validated_data)

        return Response(serializer.data, status=HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        event = Event.objects.get(id=kwargs['pk'])

        if event.owner != request.user:
            return Response({'status': 400, 'text': 'you don\'t have permission'},
                            status=HTTP_400_BAD_REQUEST)

        event.delete()

        return Response({'status': 200, 'text': 'successfully deleted'}, status=HTTP_200_OK)

    @detail_route(methods=['POST'], permission_classes=[IsAuthenticated])
    def join(self, request, pk, *args, **kwargs):
        serializer = EventJoinSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data.get('user')

        event = Event.objects.get(id=pk)

        if user in event.members.all():
            return Response({'status': 400, 'text': 'you already joined'},
                            status=HTTP_400_BAD_REQUEST)
        else:
            event.members.add(user.id)
            event.save()

        return Response({'status': 200, 'text': 'successfully joined'}, status=HTTP_200_OK)

    @detail_route(methods=['DELETE'], permission_classes=[IsAuthenticated])
    def delete_member(self, request, pk, *args, **kwargs):
        if 'user' in request.GET:
            event = Event.objects.get(id=pk)

            if event.owner.id != request.GET['user'] \
                    and event.owner == request.user:
                event.members.remove(request.GET['user'])
                event.save()
            else:
                return Response({'status': 400, 'text': 'you don\'t have permission'},
                                status=HTTP_400_BAD_REQUEST)

        return Response({'status': 200, 'text': 'user deleted from event'},
                        status=HTTP_200_OK)


