from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models

from Account.utils import PathAndRename


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if email is None:
            return ValueError('User must have email')

        user = self.model(email=email)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password)
        user.type = 0
        user.status = 1
        user.is_verified = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    class Status:
        ACTIVE = 0
        DISABLED = 1

    class Type:
        ADMIN = 0
        USER = 1
        BOT = 2

    class Gender:
        MALE = 0
        FEMALE = 1

    email = models.EmailField(max_length=250, unique=True)
    first_name = models.CharField(max_length=250, blank=True)
    last_name = models.CharField(max_length=250, blank=True)
    gender = models.IntegerField(default=Gender.MALE, blank=True)
    phone = models.CharField(max_length=250, blank=True)
    birth_date = models.DateField(blank=True, null=True)
    avatar = models.ImageField(upload_to=PathAndRename('users/'), blank=True)
    friends = models.ManyToManyField('Account.User', related_name='friends_list', blank=True)
    city = models.ForeignKey('Account.City', related_name='user_city', on_delete=models.CASCADE, blank=True, null=True)
    gallery = models.ManyToManyField('Account.Gallery', related_name='user_gallery', blank=True)
    telegram_name = models.CharField(max_length=250, blank=True, null=True)
    type = models.IntegerField(default=Type.USER)
    status = models.IntegerField(default=Status.DISABLED)
    is_verified = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return '{} - {}'.format(self.id, self.email)

    def has_perm(self, perm, obj=None):
        return self.is_staff

    def has_module_perms(self, app_label):
        return self.is_verified


class Country(models.Model):
    country_name = models.CharField(max_length=250)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.country_name


class City(models.Model):
    city_name = models.CharField(max_length=250)
    country = models.ForeignKey('Account.Country', related_name='cities', on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '{} - {}'.format(self.country_id, self.city_name)


class Gallery(models.Model):
    image = models.ImageField(upload_to=PathAndRename('gallery/'), blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return str(self.id)


class Experience(models.Model):
    owner = models.ForeignKey('Account.User', related_name='user_experience', on_delete=models.CASCADE)
    guest = models.ForeignKey('Account.User', related_name='user_guest', on_delete=models.CASCADE)
    review = models.TextField(blank=True, null=True)
    rating = models.IntegerField()

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return str(self.id)
