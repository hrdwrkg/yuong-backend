from rest_framework.routers import DefaultRouter

from Account.views import AccountViewSet, CountryViewSet, CityViewSet, GalleryViewSet, ExperienceViewSet, \
    UserSearchViewSet

router = DefaultRouter()
router.register(r'auth', AccountViewSet, base_name='auth')
router.register(r'user/search', UserSearchViewSet, base_name='user_search')
router.register(r'country', CountryViewSet, base_name='country')
router.register(r'city', CityViewSet, base_name='city')
router.register(r'gallery', GalleryViewSet, base_name='gallery')
router.register(r'experience', ExperienceViewSet, base_name='experience')