import datetime

from celery.task import periodic_task, task

from Account.models import User
from Notification.models import Notification


@task
@periodic_task(run_every=datetime.timedelta(days=7))
def check_unverified_users():
    try:
        users = User.objects.filter(is_verified=False)

        for user in users:
            Notification.objects.create(type=4, message='Verify account', send_to=user)
    except:
        pass


@task
@periodic_task(run_every=datetime.timedelta(days=30))
def delete_unverified_users():
    try:
        users = User.objects.filter(is_verified=False)

        for user in users:
            user.delete()
    except:
        pass
