from django.core.cache import cache
from drf_haystack.viewsets import HaystackViewSet
from rest_framework import viewsets, mixins
from rest_framework.authtoken.models import Token
from rest_framework.decorators import list_route
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK

from Account.email import send_email
from Account.models import User, Country, City, Gallery, Experience
from Account.serializers import UserRegisterSerializer, UserLoginSerializer, \
    UserResponseSerializer, UserEmailActivateSerializer, UserPhoneSerializer, UserPhoneValidateSerializer, \
    UserResetPasswordSerializer, UserResetCodeSerializer, UserChangePasswordSerializer, CountrySerializer, \
    CitySerializer, GallerySerializer, UserTokenSerializer, UserResetNewPasswordSerializer, ExperienceSerializer, UserSearchSerializer
from Account.smsc_api import SMSC
from Account.utils import generate_validation_code


class AccountViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin, viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserResponseSerializer
    sms_service = SMSC()

    email_timeout = 60 * 30
    message_timeout = 60 * 30
    reset_timeout = 60 * 30

    def get_permissions(self):
        if self.action in ['list', 'retrieve']:
            self.permission_classes = [AllowAny]
        elif self.action in ['update', 'partial_update']:
            self.permission_classes = [IsAuthenticated]

        return super(AccountViewSet, self).get_permissions()

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def register(self, request, *args, **kwargs):
        serializer = UserRegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data.get('email')
        password = serializer.validated_data.get('password')

        first_name = serializer.validated_data.get('first_name')
        last_name = serializer.validated_data.get('last_name')
        city = serializer.validated_data.get('city')

        gender = serializer.validated_data.get('gender')
        birth_date = serializer.validated_data.get('birth_date')

        if User.objects.filter(email=email).count() != 0:
            return Response({'status': 400, 'text': 'email already exist'}, status=HTTP_400_BAD_REQUEST)

        email_code = generate_validation_code()

        user = {
            'email': email,
            'first_name': first_name,
            'last_name': last_name,
            'city': city,
            'gender': gender,
            'birth_date': birth_date,
            'password': password,
            'email_code': email_code
        }

        cache.set('email_validation_' + email_code, user, self.email_timeout)

        email_send = send_email(email, email_code, 1)

        if email_send is False or email_send is None:
            return Response({'status': 400, 'text': 'can not send email'}, status=HTTP_400_BAD_REQUEST)

        return Response({'status': 200, 'text': 'check your email'}, status=HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def activate_email(self, request, *args, **kwargs):
        serializer = UserEmailActivateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email_code = serializer.validated_data.get('code')

        cached_user = cache.get('email_validation_' + email_code)

        if cached_user is None:
            return Response({'status': 400, 'text': 'code is expired'}, status=HTTP_400_BAD_REQUEST)

        if cached_user.get('email_code') != email_code:
            return Response({'status': 400, 'text': 'code does not match'}, status=HTTP_400_BAD_REQUEST)

        user = User.objects.create(first_name=cached_user.get('first_name'),
                                   last_name=cached_user.get('last_name'),
                                   email=cached_user.get('email'),
                                   gender=cached_user.get('gender'),
                                   birth_date=cached_user.get('birth_date'),
                                   city=cached_user.get('city'))

        user.set_password(cached_user.get('password'))
        user.save()

        cache.delete('email_validation_' + email_code)

        Token.objects.get_or_create(user=user)

        serializer = UserResponseSerializer(user)

        return Response(serializer.data, status=HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[IsAuthenticated])
    def activate_phone(self, request, *args, **kwargs):
        serializer = UserPhoneSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        phone = serializer.validated_data.get('phone')

        if User.objects.filter(phone=phone).count() != 0:
            return Response({'status': 400, 'text': 'phone already exist'}, status=HTTP_400_BAD_REQUEST)

        # message_code = generate_validation_code()
        message_code = '1403199'

        cached_data = {
            'code': message_code,
            'user': request.user
        }

        cache.set('message_code_' + phone, cached_data, self.message_timeout)

        # self.sms_service.send_sms(phone, sms_code + '- Ваш проверочный код')

        return Response({'status': 200}, status=HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[IsAuthenticated])
    def validate_code(self, request, *args, **kwargs):
        serializer = UserPhoneValidateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        phone = serializer.validated_data.get('phone')
        code = serializer.validated_data.get('code')

        cached_code = cache.get('message_code_' + phone)

        if cached_code is None or cached_code.get('user') != request.user:
            return Response({'status': 400, 'text': 'code is expired or wrong'}, status=HTTP_400_BAD_REQUEST)

        if cached_code.get('code') != code:
            return Response({'status': 400, 'text': 'code does not match'}, status=HTTP_400_BAD_REQUEST)

        request.user.is_verified = True
        request.user.phone = phone
        request.user.save()

        cache.delete('message_code_' + phone)

        return Response({'status': 200}, status=HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def login(self, request, *args, **kwargs):
        serializer = UserLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data.get('user')
        serializer = UserTokenSerializer(user)

        return Response(serializer.data, status=HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def reset_password(self, request, *args, **kwargs):
        serializer = UserResetPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data.get('email')

        if User.objects.filter(email=email).count() == 0:
            return Response({'status': 400, 'text': 'user not found'}, status=HTTP_400_BAD_REQUEST)

        cached_data = {
            'email': email
        }

        reset_code = generate_validation_code()

        cache.set('reset_code_' + reset_code, cached_data, self.reset_timeout)

        email_send = send_email(email, reset_code)

        if email_send is False or email_send is None:
            return Response({'status': 400, 'text': 'can not send email'}, status=HTTP_400_BAD_REQUEST)

        return Response({'status': 200}, status=HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def validate_reset_code(self, request, *args, **kwargs):
        serializer = UserResetCodeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        reset_code = serializer.validated_data.get('code')

        cached_reset = cache.get('reset_code_' + reset_code)

        if cached_reset is None:
            return Response({'status': 400, 'text': 'code is expired or wrong'}, status=HTTP_400_BAD_REQUEST)

        change_code = generate_validation_code()

        cache.set('change_code_' + change_code, cached_reset, self.reset_timeout)

        cache.delete('reset_code_' + reset_code)

        return Response({'change_code': change_code}, status=HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def reset_new_password(self, request, *args, **kwargs):
        serializer = UserResetNewPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        new_password = serializer.validated_data.get('new_password')
        change_code = serializer.validated_data.get('change_code')

        cached_change = cache.get('change_code_' + change_code)

        if cached_change is None:
            return Response({'status': 400, 'text': 'code is expired or wrong'}, status=HTTP_400_BAD_REQUEST)

        email = cached_change.get('email')

        user = User.objects.get(email=email)
        user.set_password(new_password)
        user.save()

        cache.delete('change_code_' + change_code)

        return Response({'status': 200}, status=HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[IsAuthenticated])
    def change_password(self, request, *args, **kwargs):
        serializer = UserChangePasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        new_password = serializer.validated_data.get('new_password')

        user = User.objects.get(email=request.user.email)

        if user != request.user:
            return Response({'status': 400}, status=HTTP_400_BAD_REQUEST)

        user.set_password(new_password)
        user.save()
        return Response({'status': 200}, status=HTTP_200_OK)

    @list_route(methods=['GET'], permission_classes=[IsAuthenticated])
    def profile(self, request, *args, **kwargs):
        return Response(data=UserResponseSerializer(instance=request.user).data, status=HTTP_200_OK)


class UserSearchViewSet(HaystackViewSet):
    index_models = [User]
    serializer_class = UserSearchSerializer
    permission_classes = [AllowAny]


class CountryViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = [AllowAny]


class CityViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = [AllowAny]


class GalleryViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin, viewsets.GenericViewSet):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    permission_classes = [AllowAny]

    def get_permissions(self):
        if self.action in ['list', 'retrieve']:
            self.permission_classes = [AllowAny]
        elif self.action in ['create', 'destroy']:
            self.permission_classes = [IsAuthenticated]

        return super(GalleryViewSet, self).get_permissions()


class ExperienceViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin,
                        mixins.DestroyModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    queryset = Experience.objects.all()
    serializer_class = ExperienceSerializer

    def get_permissions(self):
        if self.action in ['list', 'retrieve']:
            self.permission_classes = [AllowAny]
        elif self.action in ['create', 'update', 'partial_update', 'destroy']:
            self.permission_classes = [IsAuthenticated]

        return super(ExperienceViewSet, self).get_permissions()

