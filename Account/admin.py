from django.contrib import admin

from Account.models import User, Country, City, Gallery

admin.site.register(User)
admin.site.register(Country)
admin.site.register(City)
admin.site.register(Gallery)
