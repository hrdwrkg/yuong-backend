from django.utils import timezone
from haystack import indexes

from Account.models import User


class UserIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    email = indexes.CharField(model_attr='email')
    first_name = indexes.CharField(model_attr='first_name')
    last_name = indexes.CharField(model_attr='last_name')
    phone = indexes.CharField(model_attr='phone')
    friends_list = indexes.MultiValueField()
    city = indexes.CharField()

    autocomplete = indexes.EdgeNgramField()

    @staticmethod
    def prepare_autocomplete(obj):
        return " ".join((
            obj.email, obj.first_name, obj.last_name
        ))

    def prepare_friends_list(self, obj):
        return ['{} {}'.format(friend.first_name, friend.last_name)
                for friend in obj.friends_list.all()]

    def prepare_city(self, obj):
        return obj.city.city_name

    def get_model(self):
        return User

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(
            created_at__lte=timezone.now()
        )
