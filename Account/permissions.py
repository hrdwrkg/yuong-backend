from rest_framework.permissions import BasePermission


class IsSuperuser(BasePermission):

    def has_permission(self, request, view):
        return request.user.type == 0 and request.user.is_staff


class IsPhoneValidated(BasePermission):

    def has_permission(self, request, view):
        return request.user.is_verified


class IsBot(BasePermission):

    def has_permission(self, request, view):
        return request.user.type == 2
