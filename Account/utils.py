import random
import string
from uuid import uuid4

import os
from django.utils.deconstruct import deconstructible


@deconstructible
class PathAndRename(object):
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join(self.path, filename)


def generate_validation_code(length=6):
    return ''.join(["%s" % random.choice(string.digits) for item in range(length)])
