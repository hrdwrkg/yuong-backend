from django.contrib.auth import authenticate
from drf_haystack.serializers import HaystackSerializer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from Account.models import User, Country, City, Gallery, Experience
from Account.search_indexes import UserIndex
from Host.serializers import HostSerializer
from Request.serializers import RequestSerializer


class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password', 'first_name', 'last_name', 'gender', 'birth_date', 'city')


class UserEmailActivateSerializer(serializers.Serializer):
    code = serializers.CharField(required=True, max_length=12)


class UserPhoneSerializer(serializers.Serializer):
    phone = serializers.CharField(required=True, max_length=12)


class UserPhoneValidateSerializer(serializers.Serializer):
    phone = serializers.CharField(required=True, max_length=12)
    code = serializers.CharField(required=True)


class UserResponseSerializer(serializers.ModelSerializer):
    gallery = serializers.SerializerMethodField()
    requests = serializers.SerializerMethodField()
    hosts = serializers.SerializerMethodField()
    experience = serializers.SerializerMethodField()
    guest_history = serializers.SerializerMethodField()

    class Meta:
        model = User
        exclude = ('password',)

    def get_gallery(self, instance):
        serializer = GallerySerializer(instance.gallery, many=True)
        return serializer.data

    def get_requests(self, instance):
        serializer = RequestSerializer(instance.to_user, many=True)
        return serializer.data

    def get_hosts(self, instance):
        serializer = HostSerializer(instance.user_hosts, many=True)
        return serializer.data

    def get_experience(self, instance):
        serializer = ExperienceSerializer(instance.user_experience, many=True)
        return serializer.data

    def get_guest_history(self, instance):
        serializer = ExperienceSerializer(instance.user_guest, many=True)
        return serializer.data


class UserTokenSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(required=True)

    class Meta:
        model = User
        exclude = ('password',)


class UserResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class UserResetCodeSerializer(serializers.Serializer):
    code = serializers.CharField(required=True)


class UserResetNewPasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField(required=True)
    change_code = serializers.CharField(required=True)


class UserChangePasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField(required=True)


class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        user = authenticate(username=email, password=password)

        if user is not None:
            attrs['user'] = user
            return attrs
        else:
            raise ValidationError('this user can not login')


class UserSearchSerializer(HaystackSerializer):
    class Meta:
        index_classes = [UserIndex]
        fields = ('text', 'email', 'first_name', 'last_name', 'phone', 'autocomplete')


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class CountrySerializer(serializers.ModelSerializer):
    cities = serializers.SerializerMethodField()

    class Meta:
        model = Country
        fields = ('id', 'country_name', 'cities')

    def get_cities(self, instance):
        serializer = CitySerializer(instance.cities.all(), many=True)
        return serializer.data


class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = '__all__'


class ExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        fields = '__all__'

