import json
from redis import Redis

from settings.development import SESSION_REDIS


class Manager:
    class Channel:
        ADMIN_CHANNEL = 'admin_channel'
        USER_CHANNEL = 'user_channel'
        RECEIVER_CHANNEL = 'receiver_channel'

    """"
        msg = {
            event: Module.Event,
            sender: sender_id,
            receiver: receiver_id,
            data: {},
        }
    """

    @classmethod
    def send(cls, message, channel):
        redis_layer = Redis(host=SESSION_REDIS['host'], port=6379, retry_on_timeout=True)
        redis_layer.publish(channel, json.dumps(message))