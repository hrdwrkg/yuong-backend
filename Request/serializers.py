from rest_framework import serializers

from Request.models import Request


class RequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Request
        fields = '__all__'


class RequestReceiveSerializer(serializers.Serializer):
    receive = serializers.IntegerField(default=0)

    class Meta:
        model = Request
        fields = ('from_user', 'receive')


