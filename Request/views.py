from rest_framework import viewsets, mixins
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from Account.models import User
from Notification.models import Notification
from Request.models import Request
from Request.serializers import RequestSerializer, RequestReceiveSerializer


class RequestViewSet(mixins.CreateModelMixin, mixins.ListModelMixin,
                     mixins.RetrieveModelMixin, mixins.DestroyModelMixin,
                     viewsets.GenericViewSet):
    queryset = Request.objects.all()
    serializer_class = RequestSerializer

    def get_permissions(self):
        if self.action in ['create', 'list', 'retrieve', 'destroy']:
            self.permission_classes = [IsAuthenticated]
        else:
            self.permission_classes = [AllowAny]
        return super(RequestViewSet, self).get_permissions()

    def create(self, request, *args, **kwargs):
        serializer = RequestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        to_user = serializer.validated_data.get('to_user')
        message = serializer.validated_data.get('message') \
            if serializer.validated_data.get('message') is not None else None

        if Request.objects.filter(from_user=request.user, to_user=to_user).count() != 0:
            Request.objects.get(from_user=request.user, to_user=to_user).delete()
            return Response({'status': 1}, status=HTTP_200_OK)

        request_instance = Request.objects.create(from_user=request.user,
                                         to_user=to_user,
                                         message=message)
        Notification.objects.create(type=0, message="Request received", trigger_request=request_instance, send_to=to_user)

        if request_instance is None:
            return Response({'status': 0}, status=HTTP_400_BAD_REQUEST)

        serializer = RequestSerializer(request_instance)

        return Response(serializer.data, status=HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[IsAuthenticated])
    def receive_request(self, request, *args, **kwargs):
        serializer = RequestReceiveSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        from_user = serializer.validated_data.get('from_user')
        receive = serializer.validated_data.get('receive')

        user = User.objects.get(from_user=from_user)

        message = None

        if user not in request.user.friends_list.all() and receive != 1:
            request.user.friends_list.add(user)
            message = "Request accepted"
        elif user in request.user.friends_list.all():
            request.user.friends_list.remove(user)
            message = "Request canceled"

        Notification.objects.create(type=1, message=message, received_from=request.user, send_to=user)

        Request.objects.get(from_user=from_user, to_user=request.user).delete()

        return Response({'status': 0}, status=HTTP_200_OK)
