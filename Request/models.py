from django.db import models


class Request(models.Model):
    class Type:
        FRIENDSHIP = 0
        HOST = 1

    class Status:
        CREATED = 0
        ACCEPTED = 1
        CANCELED = 2

    from_user = models.ForeignKey('Account.User', related_name='from_user',
                                  on_delete=models.CASCADE, blank=True)
    to_user = models.ForeignKey('Account.User', related_name='to_user',
                                  on_delete=models.CASCADE, blank=True)
    message = models.TextField(blank=True, null=True)
    type = models.IntegerField(default=Type.FRIENDSHIP, blank=True)
    status = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '{} - {}'.format(str(self.from_user.id), str(self.to_user.id))

