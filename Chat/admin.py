from django.contrib import admin

from Chat.models import Chat, Message

admin.site.register(Chat)
admin.site.register(Message)
