from rest_framework.routers import DefaultRouter

from Chat.views import ChatViewSet, MessageViewSet

router = DefaultRouter()

router.register(r'chat', ChatViewSet, base_name='chat')
router.register(r'message', MessageViewSet, base_name='message')
