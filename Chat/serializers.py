from rest_framework import serializers

from Account.models import User
from Chat.models import Chat, Message


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = '__all__'
        extra_kwargs = {
            'request': {
                'required': True
            }
        }


class UserChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'avatar', 'email', 'type')


class ChatReadSerializer(serializers.ModelSerializer):
    last_message = serializers.SerializerMethodField(allow_null=True)

    def get_last_message(self, instance):
        message = Message.objects.filter(chat=instance.id).order_by('sent_at').last()
        if message:
            return MessageReadSerializer(message).data
        return None

    class Meta:
        model = Chat
        fields = '__all__'


class MessageReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class MessageSerializer(serializers.Serializer):
    chat = serializers.PrimaryKeyRelatedField(queryset=Chat.objects.all(), required=True)
    text = serializers.CharField(required=True)
    sent_at = serializers.DateTimeField(allow_null=True, read_only=True)