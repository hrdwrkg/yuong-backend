from haystack import indexes

from Host.models import Host
from Yuong.celery_config import timezone


class HostIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    description = indexes.CharField(model_attr='description')
    city = indexes.CharField()
    is_children_available = indexes.BooleanField(model_attr='is_children_available')
    is_pet_available = indexes.BooleanField(model_attr='is_pet_available')

    autocomplete = indexes.EdgeNgramField()

    @staticmethod
    def prepare_autocomplete(obj):
        return " ".join((
            obj.city_hosts.city_name, obj.description
        ))

    def prepare_city(self, obj):
        return obj.city_hosts.city_name

    def get_model(self):
        return Host

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(
            created_at__lte=timezone.now()
        )