from rest_framework.routers import DefaultRouter

from Travel.views import TravelViewSet

router = DefaultRouter()

router.register(r'travel', TravelViewSet, base_name='travel')