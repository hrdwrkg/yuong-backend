#!/usr/bin/env bash

docker-compose -f development.yml stop \
&& docker-compose -f development.yml rm -f \
&& docker-compose -f development.yml up -d \
&& docker-compose -f development.yml logs -f