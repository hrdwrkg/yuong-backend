#!/usr/bin/env bash

docker-compose -f production.yml stop \
&& docker-compose -f production.yml rm -f \
&& docker-compose -f production.yml up -d \
&& docker-compose -f production.yml logs -f