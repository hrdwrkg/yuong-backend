from django.db.models.signals import post_save
from django.dispatch import receiver

from Notification.models import Notification
from Realtime.managers import Manager


@receiver(post_save, sender=Notification)
def send_notification(instance, created, **kwargs):
    if created:
        Manager.send({
            "data": {
                "notification": instance,
            },
            "event": "Notification.Received"
        })
